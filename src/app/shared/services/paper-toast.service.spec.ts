import { TestBed } from '@angular/core/testing';

import { PaperToastService } from './paper-toast.service';

describe('PaperToastService', () => {
  let service: PaperToastService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaperToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
