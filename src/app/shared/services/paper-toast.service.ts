import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import pullAt from 'lodash/pullAt';
import findIndex from 'lodash/findIndex';
import { v4 as uuid } from 'uuid';

export enum PaperType {
  primary = 'primary',
  secondary = 'secondary',
  success = 'success',
  warning = 'warning',
  danger = 'danger',
  muted = 'muted'
}

export interface AlertConfig {
  id?: string;
  type: Exclude<PaperType, PaperType.primary | PaperType.muted>;
  title?: string;
  content: string;
  dismissible?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class PaperToastService {
  private _alerts = new BehaviorSubject<AlertConfig[]>([]);
  private dataStore: { alerts: AlertConfig[] } = { alerts: [] };
  public readonly alerts = this._alerts.asObservable();

  constructor() {}

  /**
   * Adds an alert to list of alert to be displayed
   */
  addAlert(alert: AlertConfig) {
    alert.id = uuid();
    alert.title = alert.title || alert.type;
    alert.dismissible = alert.dismissible || false;
    this.dataStore.alerts.push(alert);
    if (alert.dismissible) {
      this.startDismissible(alert);
    }
    const { alerts } = { ...this.dataStore };
    this._alerts.next(alerts);
  }

  startDismissible(alert: AlertConfig) {
    const { alerts } = { ...this.dataStore };
    setTimeout(() => {
      const { id } = alert;
      const index = findIndex(alerts, ['id', id]);
      console.log('removing alert ', alert, ' at ', index);
      this.removeAlertAt(index);
    }, 3000);
  }

  removeAlertAt(index: number) {
    const { alerts } = { ...this.dataStore };
    pullAt(alerts, [index]);
    this._alerts.next(alerts);
  }

  get activeAlertsCount() {
    return this.dataStore.alerts.length;
  }
}
