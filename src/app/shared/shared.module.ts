import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaperToastComponent } from './components/paper-toast/paper-toast.component';
import { PaperToastDemoComponent } from './components/paper-toast/paper-toast-demo.component';

@NgModule({
  declarations: [PaperToastComponent, PaperToastDemoComponent],
  exports: [PaperToastComponent, PaperToastDemoComponent],
  imports: [CommonModule]
})
export class SharedModule {}
