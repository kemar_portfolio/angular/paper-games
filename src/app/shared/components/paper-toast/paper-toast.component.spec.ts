import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaperToastComponent } from './paper-toast.component';

describe('PaperToastComponent', () => {
  let component: PaperToastComponent;
  let fixture: ComponentFixture<PaperToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaperToastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaperToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
