import { PaperToastService } from './../../services/paper-toast.service';
import { Component } from '@angular/core';
import { PaperType } from '../../services/paper-toast.service';

@Component({
  selector: 'paper-toast-demo',
  template: `
    <div style="display: flex;justify-content: space-evenly">
      <button
        class="btn-primary"
        (onClick)="
          onLaunchAlert({
            type: PaperType.success,
            dismissible: true
          })
        "
      >
        Launch Success Alert
      </button>

      <button
        class="btn-primary"
        (onClick)="
          onLaunchAlert({
            type: PaperType.secondary,
            dismissible: true
          })
        "
      >
        Launch Info Alert
      </button>

      <button
        class="btn-primary"
        (onClick)="
          onLaunchAlert({
            type: PaperType.warning,
            dismissible: true
          })
        "
      >
        Launch Warning Alert
      </button>

      <button
        class="btn-primary"
        (onClick)="
          onLaunchAlert({
            type: PaperType.danger,
            dismissible: true
          })
        "
      >
        Launch Danger Alert
      </button>
    </div>
  `
})
export class PaperToastDemoComponent {
  PaperType = PaperType;

  constructor(private toastService: PaperToastService) {}

  onLaunchAlert(config: any) {
    const { type, dismissible } = config;
    this.toastService.addAlert({
      type,
      content: `My ${PaperType[type]} alert ${this.toastService
        .activeAlertsCount + 1}`,
      dismissible
    });
  }
}
