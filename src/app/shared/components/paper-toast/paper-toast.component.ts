import {
  PaperToastService,
  PaperType,
  AlertConfig
} from './../../services/paper-toast.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'paper-toast',
  templateUrl: './paper-toast.component.html',
  styleUrls: ['./paper-toast.component.scss']
})
export class PaperToastComponent implements OnInit {
  PaperType = PaperType;
  alerts: AlertConfig[] = [];
  alertClasses = {
    record1showLifts: true
  };
  constructor(private toastService: PaperToastService) {}

  ngOnInit() {
    this.toastService.alerts.subscribe((alerts: AlertConfig[]) => {
      this.alerts = alerts;
    });
  }

  getAlertClass(alert: AlertConfig) {
    const { type, dismissible } = alert;
    return `alert-${PaperType[type]} ${dismissible && 'dismissible'}`;
  }

  onDismissAlert(index: number) {
    console.log('remove at ', index);
    this.toastService.removeAlertAt(index);
  }
}
