import { PaperToastService } from './../../services/paper-toast.service';
import { SharedModule } from './../../shared.module';
import { CommonModule } from '@angular/common';
import { moduleMetadata } from '@storybook/angular';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PaperToastComponent } from './paper-toast.component';

export default {
  title: 'Paper Toast',
  component: PaperToastComponent,
  decorators: [
    moduleMetadata({
      declarations: [],
      imports: [CommonModule, SharedModule],
      providers: [PaperToastService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
  ]
};
export const Alerts = () => ({
  template: `
  <div style="padding: 3rem;">
    <paper-toast></paper-toast>
     <paper-toast-demo></paper-toast-demo>
   </div>
  `
});
