import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { GameComponent } from './components/game/game.component';
import { GameMenuComponent } from './components/game-menu/game-menu.component';
import { BoardComponent } from './components/board/board.component';
import { GameScoreComponent } from './components/game-score/game-score.component';
import { StoreModule } from '@ngrx/store';
import * as fromTicTacToe from './state/tic-tac-toe.reducer';
import { EffectsModule } from '@ngrx/effects';
import { TicTacToeEffects } from './state/tic-tac-toe.effects';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: GameComponent
  }
];
@NgModule({
  declarations: [
    GameComponent,
    GameMenuComponent,
    BoardComponent,
    GameScoreComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature(
      fromTicTacToe.ticTacToeFeatureKey,
      fromTicTacToe.reducer
    ),
    EffectsModule.forFeature([TicTacToeEffects]),
    ReactiveFormsModule
  ]
})
export class TicTacToeModule {}
