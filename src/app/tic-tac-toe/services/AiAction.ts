import { Board } from '../models/board.model';

/**
 * @desc This class represents the computer player, contains a single method that uses minimax to get the best move
 * @param {Number} max_depth - limits the depth of searching
 * @param {Map} nodes_map - stores the heuristic values for each possible move
 */
export class AiAction {
  nodesMap: any;
  constructor(private maxDepth = -1) {
    this.maxDepth = maxDepth;
    this.nodesMap = new Map();
  }
  /**
   * Uses minimax algorithm to get the best move
   * @param {Object} board - an instant of the board class
   * @param {Boolean} maximizing - whether the player is a maximizing or a minimizing player
   * @param {Function} callback - a function to run after the best move calculation is done
   * @param {Number} depth - used internally in the function to increment the depth each recursive call
   * @return {Number} the index of the best move
   */
  getBestMove(
    board,
    maximizing = true,
    callback = (arg?: any) => {},
    depth = 0
  ) {
    // Throw an error if the first argument is not a board
    if (board.constructor.name !== 'Board')
      throw 'The first argument to the getBestMove method should be an instance of Board class.';

    // clear nodes_map if the function is called for a new move
    if (depth === 0) this.nodesMap.clear();

    // If the board state is a terminal one, return the heuristic value
    if (board.isTerminal() || depth === this.maxDepth) {
      if (board.isTerminal().winner === 'x') {
        return 100 - depth;
      } else if (board.isTerminal().winner === 'o') {
        return -100 + depth;
      }
      return 0;
    }

    // Current player is maximizing
    if (maximizing) {
      // Initializ best to the lowest possible value
      let best = -100;
      // Loop through all empty cells
      board.getAvailableMoves().forEach(index => {
        // Initialize a new board with the current state (slice() is used to create a new array and not modify the original)
        const child = new Board(board.state.slice());
        // Create a child node by inserting the maximizing symbol x into the current emoty cell
        child.insert('x', index);

        // Recursively calling getBestMove this time with the new board and minimizing turn and incrementing the depth
        const nodeValue = this.getBestMove(child, false, callback, depth + 1);
        // Updating best value
        best = Math.max(best, nodeValue);

        // If it's the main function call, not a recursive one, map each heuristic value with it's moves indicies
        if (depth === 0) {
          // Comma seperated indicies if multiple moves have the same heuristic value
          const moves = this.nodesMap.has(nodeValue)
            ? `${this.nodesMap.get(nodeValue)},${index}`
            : index;
          this.nodesMap.set(nodeValue, moves);
        }
      });
      // If it's the main call, return the index of the best move or a random index if multiple indicies have the same value
      if (depth === 0) {
        if (typeof this.nodesMap.get(best) == 'string') {
          var arr = this.nodesMap.get(best).split(',');
          var rand = Math.floor(Math.random() * arr.length);
          var ret = arr[rand];
        } else {
          ret = this.nodesMap.get(best);
        }

        // run a callback after calculation and return the index
        callback(ret);
        return ret;
      }
      // If not main call (recursive) return the heuristic value for next calculation
      return best;
    }

    if (!maximizing) {
      // Initializ best to the highest possible value
      let best = 100;
      // Loop through all empty cells
      board.getAvailableMoves().forEach(index => {
        // Initialize a new board with the current state (slice() is used to create a new array and not modify the original)
        let child = new Board(board.state.slice());
        // Create a child node by inserting the minimizing symbol o into the current emoty cell
        child.insert('o', index);

        // Recursively calling getBestMove this time with the new board and maximizing turn and incrementing the depth
        let nodeValue = this.getBestMove(child, true, callback, depth + 1);
        // Updating best value
        best = Math.min(best, nodeValue);

        // If it's the main function call, not a recursive one, map each heuristic value with it's moves indicies
        if (depth === 0) {
          // Comma seperated indicies if multiple moves have the same heuristic value
          var moves = this.nodesMap.has(nodeValue)
            ? this.nodesMap.get(nodeValue) + ',' + index
            : index;
          this.nodesMap.set(nodeValue, moves);
        }
      });
      // If it's the main call, return the index of the best move or a random index if multiple indicies have the same value
      if (depth === 0) {
        if (typeof this.nodesMap.get(best) == 'string') {
          var arr = this.nodesMap.get(best).split(',');
          var rand = Math.floor(Math.random() * arr.length);
          var ret = arr[rand];
        } else {
          ret = this.nodesMap.get(best);
        }

        // run a callback after calculation and return the index
        callback(ret);
        return ret;
      }
      // If not main call (recursive) return the heuristic value for next calculation
      return best;
    }
  }
}
