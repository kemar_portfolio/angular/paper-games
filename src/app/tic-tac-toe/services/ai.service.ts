import { PLAYER_TYPE, DIFFICULTY_LEVEL } from './../models/game.model';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { TicTacToeState } from '../state/tic-tac-toe.reducer';
import * as fromTicTacToe from '../state/tic-tac-toe.selectors';
import { ShadowBoardCell, PieceType, Board } from '../models/board.model';
import { makePlay } from '../state/tic-tac-toe.actions';
import { of, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { AiAction } from './AiAction';

@Injectable({
  providedIn: 'root'
})
export class AiService {
  board: any;

  constructor(private store$: Store<TicTacToeState>) {
    store$.select(fromTicTacToe.selectBoard).subscribe(board => {
      this.board = board;
    });
  }

  getEmptyCells = (board: ShadowBoardCell[] = this.board) =>
    board.filter(
      cell => cell.piece === PieceType.EMPTY && cell.player === null
    );

  takeAMasterMove(): any {
    throw new Error('Method not implemented.');
  }

  takeANoviceMove() {
    const boardState = this.board.slice().map((c: ShadowBoardCell) => c.piece);
    const board = new Board(boardState);
    const p = new AiAction();
    const aiMove = p.getBestMove(board, true);
    const cellIndex = Number(aiMove) + 1;
    const cell = this.board.filter(
      (c: ShadowBoardCell) => c.id === cellIndex
    )[0];

    return cell;
  }

  takeABlindMove(): any {
    const emptyCells = this.getEmptyCells();
    const randomCell =
      emptyCells[Math.floor(Math.random() * emptyCells.length)];
    const cell = {
      player: PLAYER_TYPE.PLAYER_TWO,
      piece: PieceType.NOUGHT,
      id: randomCell.id
    };

    this.store$.dispatch(makePlay({ cell }));

    return randomCell;
  }

  /*
   * notify the ai player that it's its turn
   * @param turn [String]: the player to play, either X or O
   *  levelOfIntelligence
   */
  getAiMove(): Observable<ShadowBoardCell> {
    return this.store$.select(fromTicTacToe.selectGameDifficulty).pipe(
      switchMap((difficulty: DIFFICULTY_LEVEL) => {
        let cell: ShadowBoardCell;
        switch (difficulty) {
          case DIFFICULTY_LEVEL.NOVICE:
            cell = this.takeANoviceMove();
            break;
          case DIFFICULTY_LEVEL.MASTER:
            cell = this.takeAMasterMove();
            break;
          default:
            cell = this.takeABlindMove();
            break;
        }
        return of(cell);
      })
    );
  }
}
