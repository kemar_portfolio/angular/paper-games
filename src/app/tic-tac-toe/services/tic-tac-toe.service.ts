import { Injectable } from '@angular/core';
import { ShadowBoardCell, PieceType } from '../models/board.model';
import clone from 'lodash/clone';
import map from 'lodash/map';
import find from 'lodash/find';
import { PLAYER_TYPE } from '../models/game.model';

@Injectable({
  providedIn: 'root'
})
export class TicTacToeService {
  static updateScore(
    winner: PLAYER_TYPE,
    score: [number, number]
  ): [number, number] {
    console.log('setting score for winner ', winner);
    if (winner === null) {
      return score;
    }
    const [playerOneScore, playerTwoScore] = score;
    if (winner === PLAYER_TYPE.PLAYER_ONE) {
      return [playerOneScore + 1, playerTwoScore];
    } else if (winner === PLAYER_TYPE.PLAYER_TWO) {
      return [playerOneScore, playerTwoScore + 1];
    }
  }

  static updateShadowBoard(board: ShadowBoardCell[], cellData: any) {
    const boardsCopy = map(board, clone);
    const cell = find(boardsCopy, { id: cellData.id });
    cell.player = cellData.player;
    cell.piece = cellData.piece;

    return boardsCopy;
  }
}
