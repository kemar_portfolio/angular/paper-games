import { PaperToastService, PaperType } from './../../shared/services/paper-toast.service';
import { AiService } from './../services/ai.service';
import { action } from '@storybook/addon-actions';
import { PLAYER_TYPE, GAME_MODE } from './../models/game.model';
import { PieceType, ShadowBoardCell } from './../models/board.model';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  switchMap,
  withLatestFrom
} from 'rxjs/operators';
import { EMPTY, of } from 'rxjs';
import * as fromTicTacToe from './tic-tac-toe.selectors';

import * as TicTacToeActions from './tic-tac-toe.actions';
import { Store, select } from '@ngrx/store';
import { TicTacToeService } from '../services/tic-tac-toe.service';
import { Board } from '../models/board.model';
import { dispatch } from 'rxjs/internal/observable/pairs';
import { makePlay } from './tic-tac-toe.actions';

@Injectable()
export class TicTacToeEffects {
  loadTicTacToes$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TicTacToeActions.loadTicTacToes),
      concatMap(() =>
        /** An EMPTY observable only emits completion. Replace with your own observable API request */
        EMPTY.pipe(
          map(data => TicTacToeActions.loadTicTacToesSuccess({ data })),
          catchError(error =>
            of(TicTacToeActions.loadTicTacToesFailure({ error }))
          )
        )
      )
    );
  });
  checkTerminal$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(TicTacToeActions.checkTerminal),
      concatMap(action =>
        of(action).pipe(
          withLatestFrom(this.store.pipe(select(fromTicTacToe.selectBoard)))
        )
      ),
      switchMap((res: any) => {
        const [action, board] = res;
        const terminalState = new Board(
          board.map((cell: ShadowBoardCell) => cell.piece)
        ).isTerminal();

        if (!terminalState) {
          return of(TicTacToeActions.advanceTurn());
        } else {
          const { winner: winningPiece } = terminalState;
          console.log('winningPiece ', winningPiece, PieceType.CROSS);
          let winner = null;
          if (winningPiece === PieceType.CROSS) {
            winner = PLAYER_TYPE.PLAYER_ONE;
          } else if (winningPiece === PieceType.NOUGHT) {
            winner = PLAYER_TYPE.PLAYER_TWO;
          }

          this.toastService.addAlert({
            type: PaperType.success,
            content: `Game Over Winner: ${winner}`,
            dismissible: true
          });

          return of(TicTacToeActions.isTerminal({ winner }));
        }
      })
    );
  });

  constructor(private actions$: Actions, private store: Store, private toastService: PaperToastService) {}
}
