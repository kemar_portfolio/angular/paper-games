import { ShadowBoardCell } from './../models/board.model';
import {
  GAME_MODE,
  PLAYER_TYPE,
  DIFFICULTY_LEVEL
} from './../models/game.model';
import { createAction, props } from '@ngrx/store';

export const loadTicTacToes = createAction('[TicTacToe] Load TicTacToes');

export const loadTicTacToesSuccess = createAction(
  '[TicTacToe] Load TicTacToes Success',
  props<{ data: any }>()
);

export const loadTicTacToesFailure = createAction(
  '[TicTacToe] Load TicTacToes Failure',
  props<{ error: any }>()
);

export const startGame = createAction(
  '[TicTacToe] Start',
  props<{ mode: GAME_MODE; difficulty: DIFFICULTY_LEVEL }>()
);

export const makePlay = createAction(
  '[TicTacToe] Make Play',
  props<{ cell: ShadowBoardCell }>()
);

export const checkTerminal = createAction('[TicTacToe] Check Terminal');

export const isTerminal = createAction(
  '[TicTacToe] Game Is Terminal',
  props<{ winner: PLAYER_TYPE }>()
);

export const advanceTurn = createAction('[TicTacToe] Advance Turn');

export const restartGame = createAction('[TicTacToe] Restart Game');

export const exitGame = createAction('[TicTacToe] Exit Game');
