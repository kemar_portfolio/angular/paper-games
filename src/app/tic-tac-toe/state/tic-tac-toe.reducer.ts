import { TicTacToeService } from './../services/tic-tac-toe.service';
import { action } from '@storybook/addon-actions';
import {
  GAME_MODE,
  PLAYER_TYPE,
  GAME_STATUS,
  DIFFICULTY_LEVEL
} from './../models/game.model';
import { Action, createReducer, on } from '@ngrx/store';
import * as TicTacToeActions from './tic-tac-toe.actions';
import { ShadowBoardCell, PieceType } from '../models/board.model';
import range from 'lodash/range';

export const ticTacToeFeatureKey = 'ticTacToe';

export interface TicTacToeState {
  mode?: GAME_MODE;
  difficulty: DIFFICULTY_LEVEL | null;
  turn: PLAYER_TYPE | null; // the player who has the turn to player
  oMovesCount: number; // the player who has the turn to player
  result: string; // the result of the game in this State
  board: ShadowBoardCell[]; // the board configuration in this state
  status: GAME_STATUS;
  winner: PLAYER_TYPE | null;
  score: [number, number];
  error?: any;
}

const board = range(1, 10).map(id => ({
  id,
  piece: PieceType.EMPTY,
  player: null
}));

export const initialState: TicTacToeState = {
  turn: null,
  oMovesCount: 0,
  status: GAME_STATUS.BEGINNING,
  result: 'still running',
  board,
  difficulty: null,
  score: [0, 0],
  winner: null,
  error: null
};
const ticTacToeReducer = createReducer(
  initialState,
  on(TicTacToeActions.startGame, (state, action) => {
    const { mode, difficulty } = action;
    return {
      ...state,
      mode,
      difficulty,
      status: GAME_STATUS.RUNNING,
      turn: PLAYER_TYPE.PLAYER_ONE
    };
  }),
  on(TicTacToeActions.makePlay, (state, action) => {
    let { board, turn } = state;
    const {
      cell: { id }
    } = action;
    board = board.map((cell: ShadowBoardCell) => {
      if (cell.id === id) {
        const piece =
          turn === PLAYER_TYPE.PLAYER_ONE ? PieceType.CROSS : PieceType.NOUGHT;
        return { ...cell, piece, player: turn };
      }
      return cell;
    });

    return { ...state, board };
  }),
  on(TicTacToeActions.advanceTurn, state => {
    let { turn } = state;
    turn =
      turn === PLAYER_TYPE.PLAYER_ONE
        ? PLAYER_TYPE.PLAYER_TWO
        : PLAYER_TYPE.PLAYER_ONE;
    return { ...state, turn };
  }),
  on(TicTacToeActions.isTerminal, (state, action) => {
    let { score } = state;
    const { winner } = action;
    score = TicTacToeService.updateScore(winner, score);
    return { ...state, winner, score, board, turn: PLAYER_TYPE.PLAYER_ONE };
  }),
  on(TicTacToeActions.restartGame, (state, action) => {
    return {
      ...state,
      winner: null,
      score: [0, 0],
      board,
      turn: PLAYER_TYPE.PLAYER_ONE
    };
  }),
  on(TicTacToeActions.exitGame, (state, action) => {
    return {
      ...initialState
    };
  })
);

export function reducer(state: TicTacToeState | undefined, action: Action) {
  return ticTacToeReducer(state, action);
}
