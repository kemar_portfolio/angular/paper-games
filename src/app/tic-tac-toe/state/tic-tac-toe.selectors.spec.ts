import * as fromTicTacToe from './tic-tac-toe.reducer';
import { selectTicTacToeState } from './tic-tac-toe.selectors';

describe('TicTacToe Selectors', () => {
  it('should select the feature state', () => {
    const result = selectTicTacToeState({
      [fromTicTacToe.ticTacToeFeatureKey]: {}
    });

    expect(result).toEqual({});
  });
});
