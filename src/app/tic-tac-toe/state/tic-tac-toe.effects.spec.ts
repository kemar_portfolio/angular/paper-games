import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { TicTacToeEffects } from './tic-tac-toe.effects';

describe('TicTacToeEffects', () => {
  let actions$: Observable<any>;
  let effects: TicTacToeEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        TicTacToeEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<TicTacToeEffects>(TicTacToeEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
