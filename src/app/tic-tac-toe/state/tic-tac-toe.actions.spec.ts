import * as fromTicTacToe from './tic-tac-toe.actions';

describe('loadTicTacToes', () => {
  it('should return an action', () => {
    expect(fromTicTacToe.loadTicTacToes().type).toBe('[TicTacToe] Load TicTacToes');
  });
});
