import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromTicTacToe from './tic-tac-toe.reducer';
import { TicTacToeState } from './tic-tac-toe.reducer';
import { PLAYER_TYPE, GAME_MODE } from '../models/game.model';

export const selectTicTacToeState = createFeatureSelector<
  fromTicTacToe.TicTacToeState
>(fromTicTacToe.ticTacToeFeatureKey);

export const selectPlayerTurn = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => state.turn
);

export const selectBoard = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => state.board
);

export const selectGameStatus = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => state.status
);

export const selectScore = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => {
    const { score } = state;
    const [playerOneScore, playerTwoScore] = score;
    return { playerOneScore, playerTwoScore };
  }
);

export const selectGameMode = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => state.mode
);
export const selectGameDifficulty = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) => state.difficulty
);

export const selectIsMachineTurn = createSelector(
  selectTicTacToeState,
  (state: TicTacToeState) =>
    state.mode === GAME_MODE.hvm && state.turn === PLAYER_TYPE.PLAYER_TWO
);
