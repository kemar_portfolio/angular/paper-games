import {
  PaperToastService,
  PaperType
} from './../../../shared/services/paper-toast.service';
import {
  GAME_MODE,
  GAME_STATUS,
  DIFFICULTY_LEVEL
} from './../../models/game.model';
import {
  startGame,
  restartGame,
  exitGame
} from './../../state/tic-tac-toe.actions';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromTicTacToe from '../../state/tic-tac-toe.selectors';

@Component({
  selector: 'paper-ttt-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.scss']
})
export class GameMenuComponent implements OnInit {
  settingsForm: FormGroup;
  GAME_MODE = GAME_MODE;
  GAME_STATUS = GAME_STATUS;
  DIFFICULTY_LEVEL = DIFFICULTY_LEVEL;
  gameStatus$ = this.store.select(fromTicTacToe.selectGameStatus);
  gameMode$ = this.store.select(fromTicTacToe.selectGameMode);
  gameDifficulty$ = this.store.select(fromTicTacToe.selectGameDifficulty);

  constructor(private store: Store, private toastService: PaperToastService) {}

  ngOnInit() {
    this.settingsForm = new FormGroup({
      mode: new FormControl('hvh', [Validators.required]),
      difficulty: new FormControl('easy', Validators.required)
    });
  }

  onStartGame() {
    const {
      value: { mode, difficulty: difficultyFormVal }
    } = this.settingsForm;
    const difficulty =
      GAME_MODE[mode] === GAME_MODE.hvm ? difficultyFormVal : null;
    this.store.dispatch(startGame({ mode: GAME_MODE[mode], difficulty }));
  }

  onRestartGame() {
    this.store.dispatch(restartGame());
  }
  onExitGame() {
    this.store.dispatch(exitGame());
  }
}
