import { PLAYER_TYPE } from './../../models/game.model';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromTicTacToe from '../../state/tic-tac-toe.selectors';

@Component({
  selector: 'paper-game-score',
  templateUrl: './game-score.component.html',
  styleUrls: ['./game-score.component.scss']
})
export class GameScoreComponent implements OnInit {
  PLAYER_TYPE = PLAYER_TYPE;
  activeTurn$ = this.store.select(fromTicTacToe.selectPlayerTurn);
  scores$ = this.store.select(fromTicTacToe.selectScore);

  constructor(private store: Store) {}

  ngOnInit() {}
}
