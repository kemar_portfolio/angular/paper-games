import { selectIsMachineTurn } from './../../state/tic-tac-toe.selectors';
import { dispatch } from 'rxjs/internal/observable/pairs';
import { AiService } from './../../services/ai.service';
import { checkTerminal } from './../../state/tic-tac-toe.actions';
import { GAME_STATUS } from './../../models/game.model';
import { PieceType } from './../../models/board.model';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromTicTacToe from '../../state/tic-tac-toe.selectors';
import { ShadowBoardCell } from '../../models/board.model';
import chunk from 'lodash/chunk';
import { makePlay } from '../../state/tic-tac-toe.actions';

interface Row {
  index: number;
  cells: ShadowBoardCell[];
}
interface BoardView {
  rows: Row[];
}

@Component({
  selector: 'paper-ttt-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  board: BoardView;
  gameStatus: GAME_STATUS;
  isMachineTurn: boolean;
  constructor(private store: Store, private aiService: AiService) {}

  ngOnInit() {
    this.store
      .select(fromTicTacToe.selectBoard)
      .subscribe((shadowBoard: ShadowBoardCell[]) => {
        this.board = this.mapToBoardView(shadowBoard);
      });
    this.store
      .select(fromTicTacToe.selectGameStatus)
      .subscribe((status: GAME_STATUS) => {
        this.gameStatus = status;
      });

    this.store
      .select(fromTicTacToe.selectIsMachineTurn)
      .subscribe((isMachineTurn: boolean) => {
        this.isMachineTurn = isMachineTurn;
        if (isMachineTurn) {
          this.handleMachinePlay();
        }
      });
  }

  handleMachinePlay() {
    setTimeout(() => {
      this.aiService.getAiMove().subscribe((cell: ShadowBoardCell) => {
        console.log('machine play ', cell);
        this.store.dispatch(makePlay({ cell }));
        setTimeout(() => {
          this.store.dispatch(checkTerminal());
        }, 1000);
      });
    }, 2000);
  }

  mapToBoardView(shadowBoard: ShadowBoardCell[]): BoardView {
    const rows = chunk(shadowBoard, 3).map((cells: ShadowBoardCell[]) => {
      return {
        cells
      };
    });

    return { rows };
  }

  onMakeCellPlay(cell: ShadowBoardCell) {
    if (this.gameStatus !== GAME_STATUS.RUNNING || this.isMachineTurn) {
      return;
    }

    this.store.dispatch(makePlay({ cell }));
    setTimeout(() => {
      this.store.dispatch(checkTerminal());
    }, 1000);
  }

  getCellContent(cell: ShadowBoardCell) {
    const { piece } = cell;
    if (piece === PieceType.EMPTY) {
      return '';
    } else {
      return piece === PieceType.CROSS ? 'X' : 'O';
    }
  }
}
