export enum PLAYER_TYPE {
  PLAYER_ONE = 'Player One',
  PLAYER_TWO = 'Player Two'
}

export enum GAME_STATUS {
  BEGINNING = 'beginning',
  RUNNING = 'running',
  ENDED = 'ended'
}

export enum GAME_MODE {
  hvh = 'Human Vs Human',
  hvm = 'Human Vs Machine'
}

export enum DIFFICULTY_LEVEL {
  EASY = 'easy',
  NOVICE = 'novice',
  MASTER = 'master'
}
