import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './components/app/app.component';
import { AppRoutingModule } from '../app-routing.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [AppComponent, NavigationComponent, HomeComponent],
  exports: [AppComponent],
  imports: [CommonModule, AppRoutingModule, SharedModule]
})
export class CoreModule {}
